

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,'Трудовое право',0,1,'2019-01-17','2019-01-17'),(2,'Гражданское право',0,1,'2019-01-18','2019-01-18'),(3,'салам',0,1,'2019-01-18','2019-01-18'),(4,'жай май',0,1,'2019-01-18','2019-01-18'),(5,'Договоры',2,1,'2019-01-18','2019-01-18'),(6,'Споры',2,0,'2019-01-18','2019-01-19'),(7,'Купля-продажа',5,1,'2019-01-18','2019-01-18'),(8,'внутри жай май2',4,0,'2019-01-19','2019-01-19');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `instructions`
--

DROP TABLE IF EXISTS `instructions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `instructions` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `is_active` tinyint(4) DEFAULT NULL,
  `cat_id` int(6) DEFAULT NULL,
  `owner_id` int(10) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `instructions`
--

LOCK TABLES `instructions` WRITE;
/*!40000 ALTER TABLE `instructions` DISABLE KEYS */;
INSERT INTO `instructions` VALUES (1,'Не выдaют зарплату',1,1,1,'2019-01-20 10:07:01','2019-01-20 16:36:50'),(2,'Спор на работе',1,1,1,'2019-01-20 10:41:22','2019-01-20 10:41:22'),(3,'как составить договор аренды',1,2,1,'2019-01-20 12:11:32','2019-01-20 12:11:32'),(4,'братец',1,7,1,'2019-01-20 15:58:04','2019-01-20 15:58:04');
/*!40000 ALTER TABLE `instructions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `steps`
--

DROP TABLE IF EXISTS `steps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `steps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `text` text,
  `ins_id` int(15) DEFAULT NULL,
  `local_id` int(5) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=78 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `steps`
--

LOCK TABLES `steps` WRITE;
/*!40000 ALTER TABLE `steps` DISABLE KEYS */;
INSERT INTO `steps` VALUES (27,'Шаг 1','',2,1,'2019-01-20 15:57:43','2019-01-20 15:57:43'),(28,'Шаг 1',NULL,3,1,'2019-01-20 15:57:53','2019-01-21 09:12:02'),(40,'Шаг 11','',4,11,'2019-01-20 15:58:39','2019-01-20 15:58:39'),(51,'Шаг 14','',4,14,'2019-01-20 16:00:13','2019-01-20 16:00:13'),(54,'Шаг 2','',3,2,'2019-01-20 16:06:20','2019-01-20 16:06:20'),(55,'очень длинное название например да это тест который нужно пройти',NULL,2,2,'2019-01-20 16:41:33','2019-01-21 17:32:09'),(70,'Шаг 1','',1,1,'2019-01-20 17:20:12','2019-01-20 17:20:12'),(73,'Шаг 4','',1,4,'2019-01-20 17:20:49','2019-01-20 17:20:49'),(74,'Шаг 5','',1,5,'2019-01-20 17:20:50','2019-01-20 17:20:50'),(75,'Шаг 3','',2,3,'2019-01-20 17:23:22','2019-01-20 17:23:22'),(76,'Шаг 4','',2,4,'2019-01-20 17:23:23','2019-01-20 17:23:23'),(77,'Шаг 3','',3,3,'2019-01-21 09:11:45','2019-01-21 09:11:45');
/*!40000 ALTER TABLE `steps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `users` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `user_type` int(2) NOT NULL,
  `login` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `surname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `tel_number` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `updated_at` date NOT NULL,
  `created_at` date NOT NULL,
  `remember_token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,1,'admin','superuser','superuser',NULL,'$2y$10$cnK8uXkKlSTiVB8JfPLgh.h3KPWib1yyHhn0OvNjhfqgcSpil/69a',1,'2018-08-09','2018-06-23','QckJP1IeWrs3rkSwpa7IStk0qEnHJWBgtODLVI9WAJmkg4luameOsowz0atH'),(14,3,'asset','Асет','Аукенов','+77086905371','$2y$10$uj11Z/sKFJuaRwmsgGC3QOFoW77T/txraO.6feJvWd5ne5mppfaXi',1,'2018-09-03','2018-09-03','k9D3l8B5gCBlPwg66S1lsjC8VBo46mcQQ5926TY9YGipApv6avGOeo5Pddah'),(15,3,'maralbek','Маралбек','Зейнуллин','+77786882978','$2y$10$ZixD0it29xlJ3EGy9Ev41./R4foimptH3a1RsMhZfliOPPV1bIe9e',1,'2018-09-03','2018-09-03','T3Lbdr1aShPSYtriq83WNukdxeNLPsuubF8KCSN8qbeD98k4GRt7iz8DshRA'),(16,2,'student','Руслан','Хузин',NULL,'$2y$10$DmRbFHnAquNcVaime8t8huxSX4NsLhnYwXpVOxxR8F1IIVqKqNGB2',1,'2018-09-03','2018-09-03','Tse2uZNcx4AfmI10sZSgukmMtnw6NX5sT2mcQwqWnTLQrhZTql9u4KopeiGo'),(18,3,'trial_test','Trial','Test','0000000000000','$2y$10$ypAhwliP8A7IllS93TxV/eoYWHweCUo/pFNsNXTLpGbeGv7EOSdE2',1,'2018-09-17','2018-09-17',NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `variants`
--

DROP TABLE IF EXISTS `variants`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `variants` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `text` varchar(255) DEFAULT NULL,
  `step_id` int(15) DEFAULT NULL,
  `link_id` int(15) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `variants`
--

LOCK TABLES `variants` WRITE;
/*!40000 ALTER TABLE `variants` DISABLE KEYS */;
INSERT INTO `variants` VALUES (27,'',54,1,'2019-01-20 16:06:25','2019-01-20 16:06:25'),(28,'',27,1,'2019-01-20 16:41:28','2019-01-20 16:41:28'),(29,'',27,1,'2019-01-20 16:41:31','2019-01-20 16:41:31'),(30,NULL,55,1,'2019-01-20 16:41:35','2019-01-21 17:32:09');
/*!40000 ALTER TABLE `variants` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-01-21 18:45:37
