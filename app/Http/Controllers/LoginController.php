<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Application;
use Hash;
use Auth;

class LoginController extends Controller{
    function getLogin (){
        
        /*
        $user = new User();
        $user->login = 'admin';
        $user->password = Hash::make('346488');
        $user->name = "Ruslan";
        $user->surname = "Khuzin";
        $user->save();
        */
        $ar = array();
        $ar['action'] = action('LoginController@postLogin');
        
        return view('superlevel.login', $ar);
    }

    function postLogin(Request $request){
        if (!Auth::attempt(['login' => $request->input('login'), 'password' => $request->input('password')]))
            return back()->with('error', 'Неверный логин/пароль');

        if (Auth::user()->user_type == 1)
            return redirect()->action('SuperLevel\SuperLevelController@getIndex', ['cat_id'=>0]);
        else if (Auth::user()->user_type == 2)
            return redirect()->action('User\UserController@getIndex', ['cat_id'=>0]);
        else if (Auth::user()->user_type == 3)
            return redirect()->action('SecondLevel\SecondLevelController@getIndex', ['cat_id'=>0]);
        else abort(404);
    }

    function getLogout(){
        Auth::logout();

        return redirect()->to('/login');
    }
}
