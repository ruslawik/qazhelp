<?php

namespace App\Http\Controllers\SecondLevel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use Auth;
use Carbon\Carbon;
use Validator;

class SecondLevelController extends Controller
{
    function getIndex (){
        $ar = array();
        $ar['title'] = "Advicer";
        $ar['username'] = Auth::user()->name;

        return view('secondlevel.main', $ar);
    }

}
