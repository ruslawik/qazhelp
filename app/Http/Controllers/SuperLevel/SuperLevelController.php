<?php
namespace App\Http\Controllers\SuperLevel;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Application;
use Hash;
use Datatables;
use Auth;
use Session;
use Validator;
use App\Category;
use App\Instruction;
use App\Step;
use App\Variant;
use App\Keyword;
use App\Post;
use App\Query;
use Storage;

class SuperLevelController extends Controller {

	public $tree_code = "";

    function getIndex ($cat_id){
    	if($cat_id == NULL){
    		$cat_id = 0;
    	}
        $ar = array();
        $ar['title'] = "QazHelp";
        $ar['username'] = Auth::user()->name;
        $ar['categories'] = Category::where('parent_id', $cat_id)
        							->get();
        $ar['now_cat'] = Category::where('id', $cat_id)
        							->get();
        $ar['cat_id'] = $cat_id;
        $ar['action'] = action("SuperLevel\SuperLevelController@addCategory");

        return view('superlevel.main', $ar);
    }

    function addCategory (Request $r){

    	$parent_id = $r->input('parent_id');
    	$new_cat_text = $r->input('jana_category');

    	$new = new Category();
    	$new->name = $new_cat_text;
    	$new->parent_id = $parent_id;
    	$new->is_active = 1;

    	$new->save();
    	return back();

    }

    function getEditCat ($cat_id){
    	 $ar = array();
         $ar['title'] = "Редактировать категорию";
    	 $ar['cat'] = Category::where('id', $cat_id)
        							->get();
         $ar['all_categories'] = Category::all();
         $ar['action'] = action('SuperLevel\SuperLevelController@saveEditedCategory');

         return view('superlevel.categories.edit', $ar);
    }

    function saveEditedCategory (Request $r){

    	$cat_id = $r->input("cat_id");
    	$parent_id = $r->input("parent_cat");
    	$jana_name = $r->input("jana_name");
    	$is_active = $r->input("is_active");

    	Category::where('id', $cat_id)->update(["parent_id"=>$parent_id, "name"=>$jana_name, "is_active"=>$is_active]);

    	return back()->with('success', 'Успешно обновлено!');
    }

    function deleteCat ($cat_id){

        $childs = Category::where("parent_id", $cat_id)->get();
        Category::where("id", $cat_id)->delete();

        $inses = Instruction::where("cat_id", $cat_id)->get();

        foreach ($inses as $ins) {

            $steps = Step::where("ins_id", $ins['id'])->get();

            foreach ($steps as $step) {
                Variant::where('step_id', $step['id'])->delete();
            }
            Step::where("ins_id", $ins['id'])->delete();
            Keyword::where('ins_id', $ins['id'])->delete();
        }

        Instruction::where("cat_id", $cat_id)->delete();

        foreach ($childs as $child) {

            $this->deleteCat($child['id']);
        }

        return back()->with('success', 'Успешно удалено!'); 
    }

    function getAllInstructions ($cat_id){

    	$ar['title'] = "Работа с инструкциями";
    	$this->build_tree(0, 0);
    	$ar['all_cats_tree'] = $this->tree_code;
    	$ar['instructions'] = Instruction::where('cat_id', $cat_id)
    										->get();
    	$now_cat = Category::where('id', $cat_id)
    						->get();
    	$ar['now_cat_name'] = $now_cat['0']['name'];
    	$ar['cat_id'] = $now_cat['0']['id'];

    	return view("superlevel.instructions.all", $ar);
    }

    function build_tree($ParentID, $lvl){

    	global $lvl; 
		$lvl++; 

		$all_cats_in_now_level = Category::where('parent_id', $ParentID)
    									->get();

		if ($all_cats_in_now_level->count() > 0) {
			$this->tree_code .= "<ul>";
			foreach ($all_cats_in_now_level as $category) {
				$ID1 = $category["id"];
				$this->tree_code .= "<li id='".$ID1."'>";
				$this->tree_code .= "<A HREF=\""."?ID=".$ID1."\">".$category["name"]."</A>";
				$this->build_tree($ID1, $lvl); 
				$lvl--;
			}
			$this->tree_code .= "</li>";
			$this->tree_code .= "</ul>";	
		}
		return ;
    }

    function getAddInstruction ($cat_id){

    	$cat = Category::where('id', $cat_id)
    					->get();

    	$ar['title'] = "Редактор инструкций";
    	$ar['action'] = action("SuperLevel\SuperLevelController@postAddInstruction");
    	$ar['cat_name'] = $cat['0']['name'];
    	$ar['cat_id'] = $cat['0']['id'];

    	return view('superlevel.instructions.add', $ar);
    }

    function editInstMainData ($ins_id){

    	$ins = Instruction::where('id', $ins_id)->get();
    	$ar['title'] = "Редактор";
    	$ar['ins'] = $ins;
    	$ar['cats'] = Category::all();
        $ar['keywords'] = Keyword::where("ins_id", $ins_id)->get();
    	$ar['action'] = action("SuperLevel\SuperLevelController@postUpdateInstruction");

    	return view('superlevel.instructions.editmaindata', $ar);
    }

    function postUpdateInstruction (Request $r){

    	$ins_id = $r->input('ins_id');
    	$ins_name = $r->input('ins_name');
    	$is_active = $r->input('is_active');
    	$new_cat = $r->input('new_cat');
        $keywords = explode(",", $r->input('keywords'));

    	Instruction::where('id', $ins_id)->update(["name" => $ins_name, "is_active" => $is_active, "cat_id" => $new_cat]);
        Keyword::where('ins_id', $ins_id)->delete();
        foreach ($keywords as $key => $value) {
            Keyword::insert(["Keyword"=>trim($value), "ins_id"=>$ins_id]);
        }
    	return redirect()->route('edit_instruction_main', ['ins_id' => $ins_id]);
        //return back();
    }

    function postAddInstruction (Request $r){

    	$ins_name = $r->input('ins_name');
    	$is_active = $r->input('is_active');
    	$cat_id = $r->input('cat_id');

    	$new_ins = new Instruction();
    	$new_ins->name = $ins_name;
    	$new_ins->is_active = $is_active;
    	$new_ins->cat_id = $cat_id;
    	$new_ins->owner_id = Auth::id();
    	$new_ins->save();

    	$new_step = new Step();
    	$new_step->text = "Первый шаг инструкции";
    	$new_step->ins_id = $new_ins->id;
    	$new_step->name = "Шаг 1";
    	$new_step->local_id = 1;
    	$new_step->save();

    	return redirect()->route('edit_instruction', ['ins_id' => $new_ins->id, 'local_id' => 1]);
    }

    function getEditInstruction ($ins_id, $local_id){

    	$ar['title'] = "Редактировать инструкцию";
    	$ins = Instruction::where('id', $ins_id)
    						->get();

    	$max_local_id = Step::where('ins_id', $ins_id)
    							->max('local_id');

    	$ar['ins_name'] = $ins['0']['name'];
    	$ar['ins_id'] = $ins['0']['id'];
    	$ar['cat_id'] = $ins['0']['cat_id'];
    	$ar['max_local_id'] = $max_local_id;
    	$ar['now_step_id'] = $local_id;

    	$ar['save_step'] = action('SuperLevel\SuperLevelController@saveStep');
    	$ar['cats'] = Category::all();
    	$ar['all_steps'] = Step::where('ins_id', $ins_id)
    					->get();

    	$ar['choosen_step'] = Step::where('ins_id', $ins_id)
    								->where('local_id', $local_id)
    								->get();


    	return view('superlevel.instructions.edit', $ar);
    }

    function getEditInstructionMain ($ins_id){
    	$step = Step::where('ins_id', $ins_id)->first();
    	if($step == NULL){
    		return redirect()->route('add_step', ['ins_id' => $ins_id, 'local_id' => 0]);
    	}
    	else{
    		return redirect()->route('edit_instruction', ['ins_id' => $ins_id, 'local_id' => $step['local_id']]); //->with('success', 'Новый шаг успешно добавлен!');
    	}
    }

    function addStep ($ins_id, $max_local_id){

    	$max_local_id++;

    	$new_step = new Step();
    	$new_step->text = "";
    	$new_step->ins_id = $ins_id;
    	$new_step->name = "Шаг ".$max_local_id;
    	$new_step->local_id = $max_local_id;
    	$new_step->save();

    	return redirect()->route('edit_instruction', ['ins_id' => $ins_id, 'local_id' => $max_local_id]); //->with('success', 'Новый шаг успешно добавлен!');
    }

    function saveStep (Request $r){

    	$step_name = $r->input('step_name');
    	$step_text = $r->input('step_text');
    	$step_id =   $r->input('step_id');

    	Step::where('id', $step_id)->update(["name" => $step_name, "text" => $step_text]);

    	$all_vars_count = $r->input("all_vars_count");

    	for($i=1; $i<=$all_vars_count; $i++){
    		$var_id = $r->input('varid_'.$i);
    		$var_text = $r->input('vartext_'.$i);
    		$var_link = $r->input('varlink_'.$i);
    		Variant::where('id', $var_id)->update(["text" => $var_text, "link_id" => $var_link]);
    	}

    	return back()->with('success', 'Успешно сохранено!');
    }

    function addVariant ($step_id){

    	$new_var = new Variant();
    	$new_var->text = "";
    	$new_var->step_id = $step_id;
    	$new_var->link_id = 1;
    	$new_var->save();

    	return back();
    }

    function delVariant ($var_id){
    	Variant::where('id', $var_id)->delete();
    	return back();
    }

    function delStep ($step_id, $ins_id){

    	Step::where('id', $step_id)->delete();
    	Variant::where('step_id', $step_id)->delete();

    	return redirect()->route('edit_instruction_main', ['ins_id' => $ins_id]); //->with('success', 'Новый шаг успешно добавлен!');
    }

    function delIns ($ins_id){

        $steps = Step::where("ins_id", $ins_id)->get();

        foreach ($steps as $step) {
                Variant::where('step_id', $step['id'])->delete();
        }
        Step::where("ins_id", $ins_id)->delete();
        Instruction::where("id", $ins_id)->delete();
        Keyword::where('ins_id', $ins_id)->delete();

        return redirect()->route('all_inses', ['cat_id' => 1])->with('success', "Успешно удалено!"); 

    }

    function allPosts (){

        $ar['title'] = "Все новости";

        $ar['posts'] = Post::all();

        return view('superlevel.news.all', $ar);
    }

    function addPost (){

        $ar['title'] = "Добавить новость";
        $ar['action'] = action('SuperLevel\SuperLevelController@postPost');

        return view('superlevel.news.add', $ar);
    }

    function postPost (Request $r){

        $val = Validator::make($r->all() ,[
            'news_photo' => 'required',
            'news_photo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'post_name' => 'required',
            'post_text' => 'required',
        ]);
        $val->validate();

        $path = explode("/", $r->file('news_photo')->store('public'));
        $post = new Post();
        $post->name = $r->input('post_name');
        $post->text = $r->input('post_text');
        $post->image = $path[1];
        $post->active = $r->input('is_active');
        $post->save();

        return back()->with('success', 'Новость успешно добавлена!');
    }

    function edit_post ($post_id){
        $ar['title'] = "Редактировать новость";
        $ar['action'] = action('SuperLevel\SuperLevelController@editPost');
        $ar["post"] = Post::where('id', $post_id)->get();

        return view('superlevel.news.update', $ar);
    }

    function editPost (Request $r){

        $val = Validator::make($r->all() ,[
            'news_photo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'post_name' => 'required',
            'post_text' => 'required',
        ]);
        $val->validate();

        if($r->hasFile('news_photo')) {
            $post_id = $r->input("post_id");
            $post = Post::where('id', $post_id)->get();
            $now_image_name = $post[0]['image'];
            Storage::delete("/public/".$now_image_name);
            $path = explode("/", $r->file('news_photo')->store('public'));
            Post::where("id", $post_id)->update(["name" => $r->input('post_name'), "text" => $r->input('post_text'), "image" => $path[1], "active" => $r->input('is_active')]);
        }else{
            $post_id = $r->input("post_id");
            Post::where("id", $post_id)->update(["name" => $r->input('post_name'), "text" => $r->input('post_text'), "active" => $r->input('is_active')]);
        }

        return back()->with("success", "Успешно обновлено!");
    }

    function queries (){
        $ar['title'] = "Все запросы";
        $ar['queries'] = Query::all();
        return view('superlevel.statistics.queries', $ar);
    }

    public function queries_ajax ()
    {
        return Datatables::of(Query::query())->make(true);
    }

}



















