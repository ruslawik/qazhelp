<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Instruction;
use App\Step;
use App\Post;
use App\Query;

class FrontController extends Controller
{
	public $tree_code = "";

    public function getIndex(){

    	$ar['title'] = "Работа с инструкциями";
    	$ar['now_cat_name'] = "Последние добавленные инструкции";
        $ar['inses'] = Instruction::where('is_active', 1)->get();
        $ar['news'] = Post::where('active', 1)->get()->take(8);

    	return view("front.index", $ar);
    }

    public function getAbout(){

        $ar['title'] = "Работа с инструкциями";

        return view("front.single", $ar);
    }

    public function getPost ($post_id){

        $post = Post::where('id', $post_id)->get();

        $ar['title'] = $post[0]['name'];
        $ar['text'] = $post[0]['text'];
        $ar['image'] = $post[0]['image'];

        return view("front.news.post", $ar);
    }

    public function getContacts (){

        $ar['title'] = "Контакты";

        return view("front.contact2", $ar);
    }

    public function getSearch(Request $r){

        $ar['title'] = "Поиск";
        $search_text = $r->get("search_text");
        if(strlen($search_text)>=5){
            $query = new Query();
            $query->query = $search_text;
            $query->save();
        }

        $ar['search_text'] = $search_text;
        $ar['result'] = Instruction::search($search_text)->where('is_active', 1)->get();
        $ar['result_step'] = Step::search($search_text)->get();

        return view("front.search", $ar);
    }

    function getInstruction ($ins_id){

        $ar['ins_data'] = Instruction::where('id', $ins_id)
                                        ->get();

        $ar['steps'] = Step::where('ins_id', $ins_id)->orderBy('local_id', 'asc')->get();


        return view("front.instruction", $ar);
    }

    public function getCategory ($cat_id){

        $cat = Category::where('id', $cat_id)->get();

        $ar['inses'] = Instruction::where('cat_id', $cat_id)->get();
        $ar['category_name'] = $cat[0]['name'];

        return view("front.category", $ar);
    }


}
