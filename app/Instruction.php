<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Instruction extends Model
{
	use FullTextSearch;

    protected $table = "instructions";

    protected $searchable = [
        'name'
    ];
}
