<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Variant;
use App\Instruction;

class Step extends Model
{
	use FullTextSearch;
	
    protected $table = "steps";

    protected $searchable = [
    	'text',
        'name'
    ];

    function all_variants (){

    	return $this->hasMany('App\Variant', 'step_id', 'id');
    }

    function ins (){
        return $this->belongsTo('App\Instruction', 'ins_id', 'id')->where('is_active', 1);
    }
}
