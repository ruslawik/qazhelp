@extends('teacher.layout')

@section('title', $title)

@section('content')

    <div class="col-sm-12">
            <div class="alert  alert-success alert-dismissible fade show" role="alert">
                <span class="badge badge-pill badge-success">Задать домашнее задание</span> Выберите студентов и впишите домашнее задание. Вы также можете прикрепить файлы к заданию.
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            @if(Session::has('message'))
            <div class="alert alert-success">
                <b>{!! Session::get("message") !!}</b>
            </div>
            @endif
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title mb-3">Задать домашнее задание</strong>
                    </div>
                    <div class="card-body">
                        <form action="{{ $action }}" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}

                        <input type="hidden" name="total_students" value="{{ $all_group_students->count() }}">
                        <textarea class="form-control" name="hometask_text" id="summary-ckeditor"></textarea>
                        
                        {!! Form::file('file1') !!}
                        {!! Form::file('file2') !!}
                        {!! Form::file('file3') !!}
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title mb-3">Группа "{{ $group['0']->group_name }}"</strong>
                    </div>
                    <div class="card-body">
                        <table id="bootstrap-data-table" class="table table-striped table-bordered">
                            <thead>
                               <tr>
                                  <td>Имя</td>
                                  <td>Фамилия</td>
                                  <td><center>Выбрать</center></td>
                               </tr>
                            </thead>
                        <tbody>
                <?php $k = 1; ?>
                @foreach ($all_group_students as $student)
                    <tr>
                        <td>{{ $student->user->name }}</td>
                        <td>{{ $student->user->surname }}</td>
                        <td><center><input type="checkbox" name="st{{ $k }}" value="{{ $student->user->id }}" checked></center></td>
                    </tr>
                    <?php $k++; ?>
                @endforeach
                </tbody>
            </table>
                <input type="submit" value="Задать" class="btn btn-success">
            </form>
            </div>
            </div>
        </div>

    </div>

@endsection

@section('editor_javascript')
<script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
<script>
    CKEDITOR.replace( 'summary-ckeditor' );
</script>
@endsection
