@extends('teacher.layout')

@section('title', $title)

@section('content')

    <div class="col-sm-12">
            <div class="alert  alert-success alert-dismissible fade show" role="alert">
                <span class="badge badge-pill badge-success">Студенты</span> Отображаются Ваши группы. Нажмите для просмотра списка студентов
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <table class="table">
            	<tr>
            		<td>Группа</td>
            		<td>Дата добавления</td>
            	</tr>
                @foreach ($groups as $group)
                    <tr>
                        <td>
                            <a href="#" onClick="show_group({{$group->id}});">{{$group->group_name}}</a>
                            <div id="gr{{$group->id}}" style="display:none;">
                                <table class="table table-striped">
                                    <tr>
                                        <td>
                                            №
                                        </td>
                                        <td>
                                            Студент
                                        </td>
                                    </tr>
                                    <?php $k = 1; ?>
                                    @foreach ($group->students as $student)
                                        <tr>
                                            <td>
                                                {{ $k }}
                                            </td>
                                            <td>
                                                <a href="/teacher/student_info/{{ $student->user->id }}">{{ $student->user->name." ".$student->user->surname  }}</a>
                                                <?php $k++; ?>
                                            </td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                        </td>
                        <td>{{$group->created_at}}</td>
                    </tr>
                @endforeach
            </table>
    </div>

@endsection

@section('datatable_js')
    <script>
        function show_group(id){

            jQuery("#gr"+id).toggle();
        }
    </script>
@endsection