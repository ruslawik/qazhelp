@extends('student.layout')

@section('title', $title)

@section('content')

    <div class="col-sm-12">
            <div class="alert  alert-success alert-dismissible fade show" role="alert">
                <span class="badge badge-pill badge-success">Ваши тестирования</span> Выберите нужный тест для того, что пройти его
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
    </div>

    <div class="col-sm-12">

    	<table class="table table-striped">
    		<thead>
    			<tr>
    				<td>Название тестирования</td>
    				<td>Крайний срок сдачи</td>
    				<td>Дата назначения</td>
    			</tr>
    		</thead>
    		<tbody>
    			@foreach($all_testings as $testing)
    				<tr>
	    				<td>
	    					@if($date <= $testing->deadline)
	    						<a href="/student/pass_test/{{ $testing->test->id }}">{{ $testing->test->name }}
	    					@else
	    						{{ $testing->test->name }}
	    					@endif
	    				</td>
    					<td>
    						@if($date <= $testing->deadline)
    							<font color="green"><b>{{ $testing->deadline }}</b></font>
    						@endif
    						@if($date > $testing->deadline)
    							<font color="red"><b>{{ $testing->deadline }}</b></font>
    						@endif
    					</td>
    					<td>{{ $testing->created_at }}</td>
    				</tr>
    			@endforeach
    		</tbody>
    	</table>
    	@if ($all_testings->count() === 0)
    		<div class="alert alert-success">
    			Вам не назначено ни одного тестирования
    		</div>
    	@endif

     </div>
    

@endsection