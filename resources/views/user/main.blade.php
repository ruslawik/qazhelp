@extends('user.layout')

@section('title', $title)

@section('content')
	<style>
		p{
			color:black !important;
		}
	</style>
    <div class="col-sm-12">
            <div class="alert  alert-success alert-dismissible fade show" role="alert">
                <span class="badge badge-pill badge-success">Приветствуем!</span> Добро пожаловать, {{ $username }}!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
    </div>

    <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title mb-3">Лента событий</strong>
                    </div>
                    <div class="card-body">

            		</div>
       			</div>
    </div>

@endsection