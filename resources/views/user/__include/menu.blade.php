<ul class="nav navbar-nav">
                    <li class="active">
                        <a href="/student/main"> <i class="menu-icon fa fa-dashboard"></i>Главная</a>
                    </li>
                    <h3 class="menu-title">Тесты</h3><!-- /.menu-title -->
                    <li><a href="/student/pass_tests"><i class="menu-icon fa fa-pencil"></i>Пройти</a></li>
                    <li><a href="/student/results_tests"><i class="menu-icon fa fa-list"></i>Результаты</a></li>

                    <h3 class="menu-title">Домашнее задание</h3><!-- /.menu-title -->
                        <li><a href="/student/hometask"><i class="menu-icon fa fa-list"></i>Просмотреть</a></li>

                    <h3 class="menu-title">Расписание</h3><!-- /.menu-title -->
                        <li><a href="/student/calendar"><i class="menu-icon fa fa-users"></i>Просмотреть</a></li>

</ul>