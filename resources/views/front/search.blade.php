@extends("front.layout")
@section("main_content")
  <br>
    <div class="site-section">
      <div class="container">
        <div class="row">
          <div class="col-lg-8">
            <br>
             <h4 class="h5 mb-4 text-black">Результаты поиска по запросу "{{$search_text}}"</h4>
             <b>По названиями инструкций:</b>
             <hr>
            @foreach($result as $ins)
              <a href="/instruction/{{$ins['id']}}">{{$ins['name']}}</a><hr>
            @endforeach
            <b>По содержданию шагов инструкций:</b>
            <hr>
            @foreach($result_step as $step)
              @if(strlen($step['text']) > 0)
                <a href="/instruction/{{$step->ins['id']}}">{{$step->ins['name']}}</a><br>
                {!!$step['text']!!}<hr>
              @endif
            @endforeach
          </div>
          <div class="col-lg-3 ml-auto">

            <div class="mb-5">
              <h3 class="h5 text-black mb-3"></h3>
              <form action="#" method="post">
                <div class="form-group">
                </div>
              </form>
            </div>


            <div class="mb-5">
              <p>Нажмите на название инструкции и следуйте ее шагам!</p>
            </div>

          </div>

        </div>
      </div>
    </div>
    <br>
  <br>
  <br>
  <br>

@endsection