<!DOCTYPE html>
<html lang="en">
  <head>
    <title>QazHelp</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css?family=Nanum+Gothic:400,700,800" rel="stylesheet">
    <link rel="stylesheet" href="/front/fonts/icomoon/style.css">

    <link rel="stylesheet" href="/front/css/bootstrap.min.css">
    <link rel="stylesheet" href="/front/css/magnific-popup.css">
    <link rel="stylesheet" href="/front/css/jquery-ui.css">
    <link rel="stylesheet" href="/front/css/owl.carousel.min.css">
    <link rel="stylesheet" href="/front/css/owl.theme.default.min.css">

    <link rel="stylesheet" href="/front/css/bootstrap-datepicker.css">

    <link rel="stylesheet" href="/front/fonts/flaticon/font/flaticon.css">

    <link rel="stylesheet" href="/front/css/aos.css">
    <link rel="stylesheet" href="/front/css/rangeslider.css">

    <link rel="stylesheet" href="/front/css/style.css">
    <!-- Yandex.Metrika counter -->
    <script type="text/javascript" >
   (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
   m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
   (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

   ym(53646913, "init", {
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true
   });
  </script>
  <!-- /Yandex.Metrika counter -->
  </head>
  <body>
  
  <div class="site-wrap">

    <div class="site-mobile-menu">
      <div class="site-mobile-menu-header">
        <div class="site-mobile-menu-close mt-3">
          <span class="icon-close2 js-menu-toggle"></span>
        </div>
      </div>
      <div class="site-mobile-menu-body"></div>
    </div>
    
    <header class="site-navbar container py-0 bg-white" role="banner">

      <!-- <div class="container"> -->
        <div class="row align-items-center">
          
          <div class="col-6 col-xl-2">
            <h1 class="mb-0 site-logo"><a href="/" class="text-black mb-0"><img src="/front/images/logo.png" width=200></a></h1>
          </div>
          <div class="col-12 col-md-10 d-none d-xl-block">
            <nav class="site-navigation position-relative text-right" role="navigation">

              <ul class="site-menu js-clone-nav mr-auto d-none d-lg-block">
                <li class="active"><a href="/">Главная</a></li>
                <li class="has-children">
                  <a href="/about">О компании</a>
                  <ul class="dropdown">
                    <li><a href="/about/#team">Команда</a></li>
                    <li><a href="/about/#mission">Миссия проекта</a></li>
                  </ul>
                </li>
                <li><a href="/contacts">Контакты и FAQ</a></li>
                <!--
                <li class="ml-xl-3 login"><a href="/userlogin"><span class="border-left pl-xl-4"></span>Войти</a></li>
                <li><a href="/userregister">Регистрация</a></li>
                !-->
                
              </ul>
            </nav>
          </div>


          <div class="d-inline-block d-xl-none ml-auto py-3 col-6 text-right" style="position: relative; top: 3px;">
            <a href="#" class="site-menu-toggle js-menu-toggle text-black"><span class="icon-menu h3"></span></a>
          </div>

        </div>
      <!-- </div> -->
      
    </header>

  

    @yield("main_content")
    
    <div class="newsletter py-5">
      <div class="container">
        <div class="row align-items-center">
          <div class="col-md-6">
          </div>
          <div class="col-md-6">
          </div>
        </div>
      </div>
    </div>
  
    
    <footer class="site-footer">
      <div class="container">
        <div class="row">
          <div class="col-md-9">
            <div class="row">
              <div class="col-md-6">
                <h2 class="footer-heading mb-4">О проекте</h2>
                <p>Основная идея проекта</p>
              </div>

              <div class="col-md-3">
                <h2 class="footer-heading mb-4">Подписаться</h2>
                <a href="#" class="pl-0 pr-3"><span class="icon-facebook"></span></a>
                <a href="#" class="pl-3 pr-3"><span class="icon-twitter"></span></a>
                <a href="#" class="pl-3 pr-3"><span class="icon-instagram"></span></a>
                <a href="#" class="pl-3 pr-3"><span class="icon-linkedin"></span></a>
              </div>
            </div>
          </div>
          <div class="col-md-3">
            <form method="GET" action="/search">
              <div class="input-group mb-3">
                <input type="text" name="search_text" class="form-control border-secondary text-white bg-transparent">
                <div class="input-group-append">
                  <input type="submit" class="btn btn-primary" value="Поиск">
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </footer>
  </div>

  <script src="/front/js/jquery-3.3.1.min.js"></script>
  <script src="/front/js/jquery-migrate-3.0.1.min.js"></script>
  <script src="/front/js/jquery-ui.js"></script>
  <script src="/front/js/popper.min.js"></script>
  <script src="/front/js/bootstrap.min.js"></script>
  <script src="/front/js/owl.carousel.min.js"></script>
  <script src="/front/js/jquery.stellar.min.js"></script>
  <script src="/front/js/jquery.countdown.min.js"></script>
  <script src="/front/js/jquery.magnific-popup.min.js"></script>
  <script src="/front/js/bootstrap-datepicker.min.js"></script>
  <script src="/front/js/aos.js"></script>
  <script src="/front/js/rangeslider.min.js"></script>

  <script src="/front/js/main.js"></script>
  @yield("js")
  </body>
</html>