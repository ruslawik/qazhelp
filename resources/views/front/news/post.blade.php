@extends("front.layout")
@section("main_content")
    <div class="site-blocks-cover inner-page-cover overlay" style="background-image: url('/storage/{{$image}}');" data-aos="fade" data-stellar-background-ratio="0.5">
      <div class="container">
        <div class="row align-items-center justify-content-center text-center">

          <div class="col-md-10" data-aos="fade-up" data-aos-delay="400">
            
            
            <div class="row justify-content-center mt-5">
              <div class="col-md-8 text-center">
                <h1>{{$title}}</h1>
                <p class="mb-0"></p>
              </div>
            </div>

            
          </div>
        </div>
      </div>
    </div>  

    <div class="site-section">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            
            <h4 class="h5 mb-4 text-black">{{$title}}</h4>
            <hr>
            {!!$text!!}
            <hr>
          </div>

        </div>
      </div>
    </div>

        </div>
      </div>
    </div>
@endsection