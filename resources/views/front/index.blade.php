@extends("front.layout")
@section("main_content")
    <div class="site-blocks-cover overlay" style="background-image: url(/front/images/hero_2.jpg);" data-aos="fade" data-stellar-background-ratio="0.5">
      <div class="container">
        <div class="row align-items-center justify-content-center text-center">

          <div class="col-md-12">
            
            
            <div class="row justify-content-center mb-4">
              <div class="col-md-8 text-center">
                <h1 class="" data-aos="fade-up">Первая правовая помощь!</h1>
                <p data-aos="fade-up" data-aos-delay="100">Введите ключевые слова Вашего вопроса</p>
              </div>
            </div>

            <div class="form-search-wrap" data-aos="fade-up" data-aos-delay="200">
              <form method="GET" action="/search">
                <div class="row align-items-center">
                  <div class="col-lg-12 mb-4 mb-xl-0 col-xl-10">
                    <input type="text" name="search_text" class="form-control rounded" id="search_text" placeholder="Ваша проблема">
                  </div>
                 
                  <div class="col-lg-12 col-xl-2 ml-auto text-right">
                    <input type="submit" class="btn btn-primary btn-block rounded" value="Поиск">
                  </div>
                  
                </div>
              </form>
            </div>

          </div>
        </div>
      </div>
    </div>  

    <div class="site-section bg-light">
      <div class="container">
        
        <div class="overlap-category mb-5">
          <div class="row align-items-stretch no-gutters">
            <div class="col-sm-6 col-md-4 mb-4 mb-lg-0 col-lg-2">
              <a href="/category/1" class="popular-category h-100">
                <br>
                <span class="icon"><span class="flaticon-mine"></span></span>
                <span class="caption mb-2 d-block">Трудовые отношения</span>
              </a>
            </div>
            <div class="col-sm-6 col-md-4 mb-4 mb-lg-0 col-lg-2">
              <a href="/category/3" class="popular-category h-100">
                <br>
                <span class="icon"><span class="flaticon-work"></span></span>
                <span class="caption mb-2 d-block">Предпринимательство</span>
              </a>
            </div>
            <div class="col-sm-6 col-md-4 mb-4 mb-lg-0 col-lg-2">
              <a href="/category/10" class="popular-category h-100">
                <br>
                <span class="icon"><span class="flaticon-head"></span></span>
                <span class="caption mb-2 d-block">Интеллектуальная собственность</span>
              </a>
            </div>
            <div class="col-sm-6 col-md-4 mb-4 mb-lg-0 col-lg-2">
              <a href="/category/4" class="popular-category h-100">
                <br>
                <span class="icon"><span class="flaticon-equal"></span></span>
                <span class="caption mb-2 d-block">Права человека</span>
              </a>
            </div>
            <div class="col-sm-6 col-md-4 mb-4 mb-lg-0 col-lg-2">
              <a href="/category/5" class="popular-category h-100">
                <br>
                <span class="icon"><span class="flaticon-familiar"></span></span>
                <span class="caption mb-2 d-block">Семейные отношения и медицина</span>
              </a>
            </div>
            <div class="col-sm-6 col-md-4 mb-4 mb-lg-0 col-lg-2">
              <a href="/category/9" class="popular-category h-100">
                <br>
                <span class="icon"><span class="flaticon-embassy"></span></span>
                <span class="caption mb-2 d-block">Административные правонарушения</span>
              </a>
            </div>
          </div>
        </div>
        
        <div class="row">
          <div class="col-12">
            <center><h2 class="h5 mb-4 text-black">Последние новости</h2></center>
          </div>
        </div>
        <div class="row">
          <div class="col-12  block-13">
            <div class="owl-carousel nonloop-block-13">
              
              @foreach($news as $post)
              <div class="d-block d-md-flex listing vertical">
                <a href="/post/{{$post['id']}}" class="img d-block" style="background-image: url('/storage/{{$post["image"]}}')"></a>
                <div class="lh-content">
                  <h3><a href="/post/{{$post['id']}}">{{$post['name']}}</a></h3>
                  <address>
                    <?php 
                      echo substr(strip_tags($post['text']), 0, 50)."...";
                    ?>
                  </address>
                </div>
              </div>
              @endforeach

            </div>
          </div>


        </div>
      </div>
    </div>
@endsection
@section("js")
  <script>
  var words = [
    @foreach($inses as $instruction)
      "{{$instruction['name']}}",
    @endforeach
  ];
  var counter = 0;
  function change_placeholder(){
    $('#search_text').attr('placeholder', words[counter]);
    counter++;
    setTimeout(change_placeholder, 2000);
  }
  setTimeout(change_placeholder, 2000);
  </script>
@endsection