@extends("front.layout")
@section("main_content")
    <div class="site-blocks-cover inner-page-cover overlay" style="background-image: url(/front/images/hero_1.jpg);" data-aos="fade" data-stellar-background-ratio="0.5">
      <div class="container">
        <div class="row align-items-center justify-content-center text-center">

          <div class="col-md-10" data-aos="fade-up" data-aos-delay="400">
            
            
            <div class="row justify-content-center mt-5">
              <div class="col-md-8 text-center">
                <h1>Наши контакты</h1>
                <p class="mb-0">Как связаться с нами</p>
              </div>
            </div>

            
          </div>
        </div>
      </div>
    </div>  

    <div class="site-section">
      <div class="container">
        <div class="row">
          <div class="col-lg-8">
            
            <h4 class="h5 mb-4 text-black">Контакты</h4>
            <hr>
            Наши контакты здесь указать
          </div>
          <div class="col-lg-3 ml-auto">

            <div class="mb-5">
              <h3 class="h5 text-black mb-3">Для Вас</h3>
              <form action="#" method="post">
                <div class="form-group">
                </div>
              </form>
            </div>


            <div class="mb-5">
              <p>Информацию о сотрудничестве сюда</p>
            </div>

          </div>

        </div>
      </div>
    </div>

        </div>
      </div>
    </div>
@endsection