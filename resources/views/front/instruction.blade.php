@extends("front.layout")
@section("main_content")
              <br>
    <div class="site-section">
      <div class="container">
        <div class="row">
          <div class="col-lg-8">
            <br>
             <h4 class="h5 mb-4 text-black">Инструкция "{{$ins_data[0]['name']}}"</h4>
             <?php
                $c = 0;
             ?>
             @foreach($steps as $step)
                @if($c!=0)
                  <div id="step{{$step['local_id']}}" style="display:none;">
                @endif
                      <b>{{$step['name']}}</b><br>
                      {!!$step['text']!!}
                      @foreach($step->all_variants as $variant)
                        <button onClick="next('{{$variant['link_id']}}');" class="btn btn-primary">{{$variant['text']}}</button><br><br>
                      @endforeach
                      <hr>
                @if($c!=0)
                  </div>
                @endif
                <?php 
                  $c++; 
                ?>
             @endforeach
          </div>

          <div class="col-lg-3 ml-auto">
            <div class="mb-5">
              <h3 class="h5 text-black mb-3"></h3>
              <form action="#" method="post">
                <div class="form-group">
                </div>
              </form>
            </div>


            <div class="mb-5">
              <p>Следуйте шагам инструкции!</p>
            </div>

          </div>

        </div>
      </div>
    </div>

        </div>
      </div>
    </div>
    <br>
@endsection
@section('js')
  <script>
    function next(step_id){
      $("#step"+step_id).css('display', 'block');
      var target = $("#step"+step_id);
      $('html, body').animate({
       scrollTop: ($(target).offset().top)
      }, 500);
    }
  </script>
@endsection