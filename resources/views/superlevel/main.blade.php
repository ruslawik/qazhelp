@extends('superlevel.layout')

@section('title', $title)

@section('content')

    <div class="col-sm-12">
            <div class="alert  alert-success alert-dismissible fade show" role="alert">
                <span class="badge badge-pill badge-success">Приветствуем!</span> Добро пожаловать, {{ $username }}!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
       </div>
             <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                            @if($cat_id != 0)                         
                                <a href="/superlevel/main/{{$now_cat['0']->parent_id}}">
                                    <button class="btn btn-warning">
                                        < назад к "{{ $now_cat['0']->name }}"
                                    </button>
                                </a>
                            @else 
                                <strong class="card-title mb-3">
                                    Категории инструкций
                                </strong>
                            @endif
                    </div>
                    <div class="card-body">
                        <form action="{{ $action }}" method="POST" class="form-inline">
                            {{ csrf_field() }}
                                <input type="hidden" name="parent_id" value="{{$cat_id}}">
                                <input type="text" name="jana_category" class="form-control col-sm-9">
                                <input type="submit" value="Добавить" class="btn btn-success form-control col-sm-3">
                        </form>
                        <table class="table table-striped" id="cats_datatable">
                            <thead>
                                <td>Наименование</td>
                                <td>Доступ</td>
                                <td>Ред.</td>
                                 @if($cat_id != 0)<td>Удалить (вместе с инструкциями)</td>@endif
                            </thead>
                            <tbody>
                        @foreach ($categories as $category)
                            <tr>
                                <td><a href="/superlevel/main/{{$category->id}}">{{ $category->name }}</a></td>
                                <td>
                                    @if($category->is_active == 1)
                                        <font color="green"><b>Активна</b></font>
                                    @endif
                                    @if($category->is_active == 0)
                                        <font color="red"><b>Неактивна</b></font>
                                    @endif
                                </td>
                                <td><a href="/superlevel/edit_cat/{{$category->id}}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></td>
                                @if($cat_id != 0)
                                <td>
                                    <a href="/superlevel/delete_cat/{{$category->id}}"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                </td>
                                @endif
                            </tr>
                        @endforeach
                            </tbody>
                        </table>
                    </div>

@endsection

@section('datatable_js')
    <script src="/manage_res/assets/js/lib/data-table/datatables.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/dataTables.bootstrap.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/dataTables.buttons.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/buttons.bootstrap.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/jszip.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/pdfmake.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/vfs_fonts.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/buttons.html5.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/buttons.print.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/buttons.colVis.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/datatables-init.js"></script>
    <script>
    jQuery(document).ready( function () {
        jQuery('#cats_datatable').DataTable();
    });
    </script>
@endsection

