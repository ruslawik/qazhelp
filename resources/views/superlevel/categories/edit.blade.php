@extends('superlevel.layout')

@section('title', $title)

@section('content')
         <div class="col-sm-12">
            <div class="alert  alert-success alert-dismissible fade show" role="alert">
                <span class="badge badge-pill badge-success">Ред.</span> Вы можете изменить категорию либо сделать ее неактивной, при этом инструкции не будут удалены
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
           @if (\Session::has('success'))
                <div class="alert alert-success">
                    <ul>
                        <li>{!! \Session::get('success') !!}</li>
                    </ul>
                </div>
            @endif
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title mb-3">Категория "{{ $cat['0']->name }}"</strong>
                    </div>
                    <div class="card-body">
                        <form action="{{ $action }}" method="POST">
                            {{ csrf_field() }}
                                <input type="hidden" name="cat_id" value="{{$cat['0']->id}}">
                                Название:
                                <input type="text" value="{{$cat['0']->name}}" placeHolder="Новое название категории" name="jana_name" class="form-control col-sm-5">
                                <br>
                                Категория: 
                                <select name="is_active" class="form-control col-sm-5">
                                    @if($cat['0']->is_active == 1)
                                        <option value="1">Активна</option>
                                        <option value="0">Неактивна</option>
                                    @else
                                        <option value="0">Неактивна</option>
                                        <option value="1">Активна</option>
                                    @endif
                                </select> 
                                <br>
                                Родительская категория:
                                <select name="parent_cat" class="form-control col-sm-5">
                                @foreach($all_categories as $category)
                                    @if($category->id == $cat['0']->parent_id)
                                        <option value="{{$cat['0']->parent_id}}">{{$category->name}}</option>
                                    @endif
                                @endforeach
                                        <option value="0">Нет родительской</option>
                                @foreach($all_categories as $category)
                                    @if($category->id != $cat['0']->parent_id)
                                        <option value="{{$category->id}}">{{$category->name}}</option>
                                    @endif
                                @endforeach
                                </select>
                                <hr>
                                <input type="submit" value="Изменить" class="btn btn-success form-control col-sm-3">

                        </form>
                    </div>
                </div>
            </div>
        

@endsection