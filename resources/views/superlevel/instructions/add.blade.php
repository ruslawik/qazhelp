@extends('superlevel.layout')

@section('title', $title)

@section('content')
        <div class="col-lg-12">
            <div class="alert  alert-success alert-dismissible fade show" role="alert">
                <span class="badge badge-pill badge-success">Новая</span> Добавление новой инструкции
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
        </div>
           @if (\Session::has('success'))
                <div class="alert alert-success">
                    <ul>
                        <li>{!! \Session::get('success') !!}</li>
                    </ul>
                </div>
            @endif
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Вы добавляете инструкцию в категорию "{{$cat_name}}"</strong>
                        <a href="{{ url()->previous() }}"><button class="btn btn-success" style="float:right;">Назад</button></a>
                    </div>
                    <div class="card-body">
                        <form action="{{$action}}" method="POST">
                            {{ csrf_field() }}
                            <input type="hidden" name="cat_id" value="{{$cat_id}}">
                            Название новой инструкции
                            <input type="text" class="form-control" placeHolder="Введите название" name="ins_name">
                            <br>
                            Доступ к инструкции
                            <select name="is_active" class="form-control">
                                <option value="1">Активна</option>
                                <option value="0">Неактивна</option>
                            </select>
                            <br>
                            <input type="submit" value="Добавить!" class="btn btn-success">
                        </form>
                    </div>
                </div>
            </div>
@endsection