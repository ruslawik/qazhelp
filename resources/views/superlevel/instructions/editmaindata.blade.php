@extends('superlevel.layout')

@section('title', $title)

@section('content')
        <div class="col-lg-12">
            <div class="alert  alert-success alert-dismissible fade show" role="alert">
                <span class="badge badge-pill badge-success">Ред.</span> Изменить информацию
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
        </div>
           @if (\Session::has('success'))
                <div class="alert alert-success">
                    <ul>
                        <li>{!! \Session::get('success') !!}</li>
                    </ul>
                </div>
            @endif
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Редактируйте название и статус "{{$ins['0']['name']}}"</strong>
                        <a href="{{ url()->previous() }}"><button class="btn btn-success" style="float:right;">Назад</button></a>
                    </div>
                    <div class="card-body">
                        <form action="{{$action}}" method="POST">
                            {{ csrf_field() }}
                            <input type="hidden" name="ins_id" value="{{$ins['0']['id']}}">
                            Новое название инструкции
                            <input type="text" value="{{$ins['0']['name']}}" class="form-control" placeHolder="Введите название" name="ins_name">
                            <br>
                            Доступ к инструкции
                            <select name="is_active" class="form-control">
                                @if($ins['0']['is_active'] == 1)
                                        <option value="1">Активна</option>
                                        <option value="0">Неактивна</option>
                                @else
                                        <option value="0">Неактивна</option>
                                        <option value="1">Активна</option>
                                @endif
                            </select>
                            <br>
                            Новая категория
                            <select name="new_cat" class="form-control">
                                @foreach($cats as $cat)
                                    @if($cat['id'] == $ins['0']['cat_id'])
                                        <option value="{{$cat['id']}}">{{$cat['name']}}</option>
                                    @endif
                                @endforeach
                                @foreach($cats as $cat)
                                    @if($cat['id'] != $ins['0']['cat_id'])
                                        <option value="{{$cat['id']}}">{{$cat['name']}}</option>
                                    @endif
                                @endforeach
                            </select>
                            <br>
                            <?php $counter = 1; ?>
                            Введите ключевые слова через запятую:
                            <input type="text" class="form-control" name="keywords" value="@foreach($keywords as $keyword){{$keyword['keyword']}}@if($counter!=count($keywords)), @endif<?php $counter++;?>@endforeach">
                            <br>
                            <input type="submit" value="Изменить" class="btn btn-success">
                        </form>
                    </div>
                </div>
            </div>
@endsection