@extends('superlevel.layout')

@section('title', $title)

@section('content')
        <div class="col-lg-12">
            <div class="alert  alert-success alert-dismissible fade show" role="alert">
                <span class="badge badge-pill badge-success">Редактор</span> Добавляйте шаги, изменяйте текст, прикрепляйте картинки, юридические документы и указания мест на карте!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
        </div>
           @if (\Session::has('success'))
            <div class="col-lg-12">
                <div class="alert  alert-success alert-dismissible fade show" role="alert">
                    <span class="badge badge-pill badge-success">Успех!</span> {!! \Session::get('success') !!}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>

            @endif
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <form action="{{$save_step}}" method="POST">
                            <input type="hidden" name="step_id" value="{{$choosen_step['0']['id']}}">
                            <input type="hidden" name="all_vars_count" value="{{$choosen_step['0']->all_variants->count()}}">
                            {{ csrf_field() }}
                            <a href="/superlevel/del_ins/{{$ins_id}}"><i class="fa fa-trash"></i></a>&nbsp;
                            <strong class="card-title">Вы редактируете инструкцию "{{$ins_name}}"</strong>
                            <a href="/superlevel/edit_ins_maindata/{{$ins_id}}"><i class="fa fa-pencil"></i></a>
                            <input type="submit" class="btn btn-success" value="Сохранить шаг" style="float:right;">
                    </div>
                    <div class="card-body">
                        <div class="col-sm-3">
                        
                        @foreach ($all_steps as $step)
                            <a href="/superlevel/del_step/{{$step['id']}}/{{$ins_id}}" style="float:left !important; color:black; padding:10px;" id="delete_instruction">
                                   <i class="fa fa-trash"></i>
                            </a>
                            <a href="/superlevel/edit_instruction/{{$ins_id}}/{{$step['local_id']}}">
                                    <p style="padding:10px; margin:0px !important;@if($step['local_id'] == $now_step_id) background: #D9ECDB !important; color:black; @endif ">{{$step['name']}}</p>
                            </a>
                        @endforeach
                        <br>
                        <center><a href="/superlevel/add_step/ins/{{$ins_id}}/{{$max_local_id}}"><button type="button" class="btn btn-success"><i class="fa fa-magic"></i>&nbsp; Добавить шаг</button></a></center>
                    
                        </div>
                        <div class="col-sm-9">
                                <input type="text" name="step_name" value="{{$choosen_step['0']['name']}}" class="form-control">
                                <textarea class="form-control" name="step_text" id="summary-ckeditor">{{$choosen_step['0']['text']}}</textarea>
                                <br>
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>Текст</th>
                                        <th>Перейти при нажатии на</th>
                                    </tr>
                                </thead>
                                <?php
                                    $k = 1;
                                ?>
                                @foreach($choosen_step['0']->all_variants as $variant)
                                    <tr>
                                        <td>
                                            <input type="hidden" name="varid_{{$k}}" value="{{$variant['id']}}">
                                            <div class="input-group">
                                                <a href="/superlevel/del_variant/{{$variant['id']}}" class="input-group-addon"><i class="fa fa-trash"></i></a>
                                                <input type="text" name="vartext_{{$k}}" value="{{$variant['text']}}" class="form-control">
                                            </div>
                                        </td>
                                        <td>
                                        <select name="varlink_{{$k}}" class="form-control">
                                            @foreach ($all_steps as $step)
                                                @if($step['local_id'] == $variant['link_id'])
                                                    @if($step['local_id'] != $choosen_step['0']['local_id'])
                                                        <option value="{{$step['local_id']}}">{{$step['name']}}</option>
                                                    @endif
                                                @endif
                                            @endforeach
                                            @foreach ($all_steps as $step)
                                                @if($step['local_id'] != $variant['link_id'])
                                                    @if($step['local_id'] != $choosen_step['0']['local_id'])
                                                        <option value="{{$step['local_id']}}">{{$step['name']}}</option>
                                                    @endif
                                                @endif
                                            @endforeach
                                        </select>
                                        </td>
                                    </tr>
                                    <?php
                                        $k++;
                                    ?>
                                @endforeach
                                </table>
                                <br>
                                <center><a href="/superlevel/add_variant/{{$choosen_step['0']['id']}}"><button type="button" class="btn btn-success"><i class="fa fa-plus"></i>&nbsp; Добавить вариант</button></a></center>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
@endsection

@section('datatable_js')
    <script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
    <script>
        CKEDITOR.replace( 'summary-ckeditor' );
    </script>
@endsection