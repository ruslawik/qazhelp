@extends('superlevel.layout')

@section('title', $title)

@section('content')
        <div class="col-lg-12">
            <div class="alert  alert-success alert-dismissible fade show" role="alert">
                <span class="badge badge-pill badge-success">Инструкции</span> Вы можете выбрать нужную категорию и добавить в нее инструкцию 
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
        </div>
           @if (\Session::has('success'))
                <div class="alert alert-success">
                    <ul>
                        <li>{!! \Session::get('success') !!}</li>
                    </ul>
                </div>
            @endif
            <div class="col-sm-4">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Выберите</strong>
                    </div>
                    <div class="card-body">
                        <div id="jstree_demo_div">
                            {!! $all_cats_tree !!}
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-8">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Инструкции категории "{{ $now_cat_name }}"</strong>
                        <a href="/superlevel/add_instruction/cat/{{$cat_id}}"><button type="button" class="btn btn-success" style="float:right;"><i class="fa fa-magic"></i>&nbsp; Добавить новую</button></a>
                    </div>
                    <div class="card-body">
                        <table class="table table-striped" id="docs_datatable">
                            <thead>
                                <td><b>Наименование</b></td>
                                <td><b>Доступ</b></td>
                            </thead>
                            <tbody>
                        @foreach ($instructions as $instruction)
                            <tr>
                                <td><a href="/superlevel/edit_instruction_main/{{$instruction->id}}">{{ $instruction->name }}</a></td>
                                <td>
                                    @if($instruction->is_active == 1)
                                        <font color="green"><b>Активна</b></font>
                                    @endif
                                    @if($instruction->is_active == 0)
                                        <font color="red"><b>Неактивна</b></font>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
@endsection

@section('datatable_js')
    <link rel="stylesheet" href="/manage_res/assets/themes/default/style.min.css" />
    <script src="/manage_res/assets/js/lib/data-table/datatables.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/dataTables.bootstrap.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/dataTables.buttons.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/buttons.bootstrap.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/jszip.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/pdfmake.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/vfs_fonts.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/buttons.html5.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/buttons.print.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/buttons.colVis.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/datatables-init.js"></script>
    <script src="/manage_res/assets/js/jstree.min.js"></script>
    <script>
        jQuery(function () { jQuery('#jstree_demo_div').jstree({
            "core" : {
            "check_callback" : true
        },
            "checkbox" : {
                "keep_selected_style" : false
        },
        "contextmenu":{         
                        "items": function($node) {
                            var tree = jQuery("#jstree_demo_div").jstree(true);
                            return {
                                "Изменить название": {
                                "separator_before": false,
                                "separator_after": false,
                                "label": "Изменить название",
                                "action": function (obj) {
                                          //$node = tree.jstree('create_node', $node);
                                          //$('#jstree_div').jstree("edit", $node);
                                        tree.edit($node, null, function(node,status){
                                            update_cat(arguments[0]['text'], arguments[0]['id']);
                                        });
                                    }
                                },
                            }
                        }
        },
            "plugins" : [ "contextmenu" ]
            });   
        });
  
        jQuery('#jstree_demo_div').on('ready.jstree', function() {
            jQuery("#jstree_demo_div").jstree("open_all"); 
        });

        jQuery('#jstree_demo_div').on('changed.jstree', function (e, data) {
            var i, j, r = [];
            for(i = 0, j = data.selected.length; i < j; i++) {
                r.push(data.instance.get_node(data.selected[i]).li_attr.id);
            }
            // r - id cat
            location = "/superlevel/all_instructions/"+r;
        });
        jQuery(document).ready( function () {
            jQuery('#docs_datatable').DataTable();
        });
    </script>
@endsection