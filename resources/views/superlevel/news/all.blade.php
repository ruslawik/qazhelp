@extends('superlevel.layout')

@section('title', $title)

@section('content')
        <div class="col-lg-12">
            <div class="alert  alert-success alert-dismissible fade show" role="alert">
                <span class="badge badge-pill badge-success">Инструкции</span> Вы можете добавить новость 
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
        </div>
           @if (\Session::has('success'))
                <div class="alert alert-success">
                    <ul>
                        <li>{!! \Session::get('success') !!}</li>
                    </ul>
                </div>
            @endif

            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Все новости</strong>
                        <a href="/superlevel/add_post"><button type="button" class="btn btn-success" style="float:right;"><i class="fa fa-magic"></i>&nbsp; Добавить новость</button></a>
                    </div>
                    <div class="card-body">
                        <table class="table table-striped" id="docs_datatable">
                            <thead>
                                <td><b>Наименование</b></td>
                                <td><b>Дата публикации</b></td>
                            </thead>
                            <tbody>
                        @foreach ($posts as $post)
                            <tr>
                                <td><a href="/superlevel/edit_post/{{$post->id}}">{{ $post->name }}</a></td>
                                <td>
                                    {{$post->created_at}}
                                </td>
                            </tr>
                        @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
@endsection

@section('datatable_js')
    <link rel="stylesheet" href="/manage_res/assets/themes/default/style.min.css" />
    <script src="/manage_res/assets/js/lib/data-table/datatables.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/dataTables.bootstrap.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/dataTables.buttons.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/buttons.bootstrap.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/jszip.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/pdfmake.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/vfs_fonts.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/buttons.html5.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/buttons.print.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/buttons.colVis.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/datatables-init.js"></script>
    <script src="/manage_res/assets/js/jstree.min.js"></script>
    <script>
        jQuery(document).ready( function () {
            jQuery('#docs_datatable').DataTable();
        });
    </script>
@endsection