@extends('superlevel.layout')

@section('title', $title)

@section('content')
        <div class="col-lg-12">
            <div class="alert  alert-success alert-dismissible fade show" role="alert">
                <span class="badge badge-pill badge-success">Инструкции</span> Вы можете добавить новость 
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
        </div>
                @foreach ($errors->all() as $error)
                    <div class="alert alert-danger">{{ $error }}</div>
                @endforeach
           @if (\Session::has('success'))
                <div class="alert alert-success">
                    {{\Session::get('success')}}
                </div>
            @endif

            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Добавить</strong>
                    </div>
                    <div class="card-body">
                        <form action="{{$action}}" method="POST" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            Название новости
                            <input type="text" value="" class="form-control" placeHolder="Введите название" name="post_name">
                            <br>
                            Опубликовано
                            <select name="is_active" class="form-control">
                                        <option value="1">Активна</option>
                                        <option value="0">Неактивна</option>
                            </select>
                            <br>
                            {{Form::label('news_photo', 'Обложка новости',['class' => 'control-label'])}}
                            <br>
                            {{Form::file('news_photo', ['class' => 'form-control'])}}
                            <br><br>
                            <textarea class="form-control" name="post_text" id="summary-ckeditor"></textarea><br>
                            <input type="submit" value="Добавить" class="btn btn-success">
                        </form>
                    </div>
                </div>
            </div>
@endsection

@section('datatable_js')
    <link rel="stylesheet" href="/manage_res/assets/themes/default/style.min.css" />
    <script src="/manage_res/assets/js/lib/data-table/datatables.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/dataTables.bootstrap.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/dataTables.buttons.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/buttons.bootstrap.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/jszip.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/pdfmake.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/vfs_fonts.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/buttons.html5.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/buttons.print.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/buttons.colVis.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/datatables-init.js"></script>
    <script src="/manage_res/assets/js/jstree.min.js"></script>
    <script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
    <script>
        CKEDITOR.replace( 'summary-ckeditor' );
    </script>
    <script>
        jQuery(document).ready( function () {
            jQuery('#docs_datatable').DataTable();
        });
    </script>
@endsection