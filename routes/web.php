<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
	Route::get('', 'FrontController@getIndex');
	Route::get('/about', 'FrontController@getAbout');
	Route::any('/search', 'FrontController@getSearch');


	Route::get('/category/{cat_id}', 'FrontController@getCategory');
	Route::get('/instruction/{ins_id}', 'FrontController@getInstruction');
	Route::any('/post/{post_id}', 'FrontController@getPost');
	Route::any('contacts', 'FrontController@getContacts');


	Route::get('login', 'LoginController@getLogin');
	Route::post('login', 'LoginController@postLogin');
	Route::any('logout', 'LoginController@getLogout');

	//Защита админки посредником через роль аккаунта из БД (админ - 1, юзер - 2)
	Route::group(['middleware' => ['auth.superlevel'], 'prefix' => 'superlevel'], function () {
		Route::any('main/{cat_id}', 'SuperLevel\SuperLevelController@getIndex');
		Route::get('all_instructions/{cat_id}', 'SuperLevel\SuperLevelController@getAllInstructions')->name('all_inses');

		Route::post('add_category', 'SuperLevel\SuperLevelController@addCategory');
		Route::get('edit_cat/{cat_id}', 'SuperLevel\SuperLevelController@getEditCat');
		Route::get('delete_cat/{cat_id}', 'SuperLevel\SuperLevelController@deleteCat');
		Route::post('save_edited_cat', 'SuperLevel\SuperLevelController@saveEditedCategory');

		Route::get('load_docs', 'SuperLevel\SuperLevelController@loadDocs');

		Route::get('add_instruction/cat/{cat_id}', 'SuperLevel\SuperLevelController@getAddInstruction');
		Route::post('add_instruction_post', 'SuperLevel\SuperLevelController@postAddInstruction');
		Route::get('edit_instruction/{ins_id}/{local_id}', 'SuperLevel\SuperLevelController@getEditInstruction')->name('edit_instruction');
		Route::get('edit_instruction_main/{ins_id}', 'SuperLevel\SuperLevelController@getEditInstructionMain')->name('edit_instruction_main');
		Route::get('edit_ins_maindata/{ins_id}', 'SuperLevel\SuperLevelController@editInstMainData');
		Route::post('post_update_ins_maindata', 'SuperLevel\SuperLevelController@postUpdateInstruction');
		Route::get('del_ins/{ins_id}', 'SuperLevel\SuperLevelController@delIns');

		Route::post('save_step', 'SuperLevel\SuperLevelController@saveStep');
		Route::get('add_step/ins/{ins_id}/{max_local_id}', 'SuperLevel\SuperLevelController@addStep')->name('add_step');

		Route::get('add_variant/{step_id}', 'SuperLevel\SuperLevelController@addVariant');
		Route::get('del_variant/{var_id}', 'SuperLevel\SuperLevelController@delVariant');

		Route::get('del_step/{step_id}/{ins_id}', 'SuperLevel\SuperLevelController@delStep');

		Route::any('all_posts', 'SuperLevel\SuperLevelController@allPosts');
		Route::any('add_post', 'SuperLevel\SuperLevelController@addPost');
		Route::any('edit_post/{post_id}', 'SuperLevel\SuperLevelController@edit_post');
		Route::post('post_a_post', 'SuperLevel\SuperLevelController@postPost');
		Route::post('edit_post', 'SuperLevel\SuperLevelController@editPost');
		Route::any('queries', 'SuperLevel\SuperLevelController@queries');
		Route::any('queries_ajax', 'SuperLevel\SuperLevelController@queries_ajax');
	});

	//Для учителя через посредник, проверяющий на роль учителя и авторизацию
	Route::group(['middleware' => ['auth.secondlevel'], 'prefix' => 'secondlevel'], function () {
		Route::any('main', 'SecondLevel\SecondLevelController@getIndex');

	});

	//Для обычного юзера роуты через простой посредник, проверяющий только авторизацию
	Route::group(['middleware' => ['auth.user'], 'prefix' => 'user'], function () {
		Route::any('main', 'User\UserController@getIndex');

	});